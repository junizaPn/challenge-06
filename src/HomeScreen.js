import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'

const HomeScreen = ({ navigation }) => {
    return (
        <View>
            <Text>HomeScreen</Text>
            <TouchableOpacity
                style={styles.buttonLg}
                onPress={() => navigation.navigate('MapsScreen')}
            >
                <Text style={styles.textLogin}>Maps</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.buttonLg}
                onPress={() => navigation.navigate('QrCodeScreen')}
            >
                <Text style={styles.textLogin}>QRCode</Text>
            </TouchableOpacity>
        </View>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    buttonLg: {
        backgroundColor: '#fff',
        borderColor: 'peru',
        borderWidth: 3,
        borderRadius: 25,
        padding: 6,
        marginVertical: 2,
        marginHorizontal: 100,
    },
    textLogin: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 15,
        alignSelf: 'center',
    },
})