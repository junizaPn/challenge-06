import React, { useEffect } from 'react';
import { View, Button } from 'react-native';
import crashlytics from '@react-native-firebase/crashlytics';
import analytics from '@react-native-firebase/analytics';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import LoginScreen from './LoginScreen';
import MapsScreen from './MapsScreen';
import QrCodeScreen from './QrCodeScreen';

const Stack = createNativeStackNavigator();
export default function App() {
  const routeNameRef = React.useRef();
  const navigationRef = React.useRef();
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={LoginScreen}>
        <Stack.Screen name="LoginScreen" component={LoginScreen} options={{ headerShown: false}} />
        <Stack.Screen name="MapsScreen" component={MapsScreen} options={{ headerShown: false}} />
        <Stack.Screen name="QrCodeScreen" component={QrCodeScreen} options={{ headerShown: false}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}