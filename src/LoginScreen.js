import { StyleSheet, Text, View, Button, TextInput, TouchableOpacity } from 'react-native'
import React from 'react'

const LoginScreen = ({navigation}) => {
    return (
        <View>
            <Text style={styles.textTitle}>TESTAPP</Text>
            <TextInput
                style={styles.textInput}
                // value={email}
                placeholder="Email"
                placeholderTextColor="black"
            />
            <TextInput
                style={styles.textInput}
                // value={password}
                placeholder="Password"
                placeholderTextColor="black"
            />
            <TouchableOpacity>
                <Text style={styles.textForgot}>Forgot Password?</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.buttonLogin}
            >
                <Text style={styles.textLogin}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.buttonLg}
            >
                <Text style={styles.textLogin}>Login with Google</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.buttonLg}
                onPress={() => navigation.navigate('MapsScreen')}
            >
                <Text style={styles.textLogin}>Maps</Text>
            </TouchableOpacity>
        </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    textTitle: {
        fontSize: 20,
        marginHorizontal: 5,
        marginVertical: 10,
        color: 'sienna',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    textInput: {
        backgroundColor: 'beige',
        borderColor: 'peru',
        borderWidth: 3,
        borderRadius: 12,
        padding: 13,
        marginVertical: 15,
        marginHorizontal: 40,
        fontSize: 18
    },
    buttonLogin:{
        backgroundColor: '#fff',
        borderColor: 'peru',
        borderWidth: 3,
        borderRadius: 25,
        padding: 6,
        marginVertical: 15,
        marginHorizontal: 100,
    },
    textLogin:{
        color: 'black',
        fontWeight: 'bold',
        fontSize: 20,
        alignSelf: 'center',
    },
    textForgot:{
        marginLeft: 250,
        color: 'black',
    },
    buttonLg:{
        backgroundColor: '#fff',
        borderColor: 'peru',
        borderWidth: 3,
        borderRadius: 25,
        padding: 6,
        marginVertical: 2,
        marginHorizontal: 100,
    }
})